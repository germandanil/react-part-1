import React from "react"
export default class MessageInput extends React.Component{
    constructor(props) {
      super(props);
      this.state = {
        value: this.props.startText? this.props.startText:"",
      };
  
      this.handleChange = this.handleChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleChange(event) {
      this.setState({value: event.target.value});
    }
  
    handleSubmit(event) {
      const date = (new Date()).toString()
      this.props.submitText(this.state.value,date)
      event.preventDefault();
    }
    

    
    render() {
      return (
          <div className = "message-input">
            <form onSubmit={this.handleSubmit}>
            <label>
                <textarea className = "message-input-text" value={this.state.value} onChange={this.handleChange} >
                  {this.props.startText}
                </textarea>
            </label>
            <input className = "message-input-button" type="submit" value={this.props.editStatus?"Edit":"Send"} />
            </form>
        </div>
      );
    }
  }