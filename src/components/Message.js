import React from "react";

export default class Message extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            isLiked: false,
        }
        this.onClick = this.onClick.bind(this);
        this.deleteMessage = this.deleteMessage.bind(this);
        this.selectMessageForEdit = this.selectMessageForEdit.bind(this);
    }
    onClick(){

        this.setState({isLiked: !this.state.isLiked});
    }
    converDate(date){
        const hoursZero = date.getHours()<10?"0":""
        const minutesZero = date.getMinutes()<10?"0":""
        return `${hoursZero+date.getHours()}:${minutesZero+date.getMinutes()}`
    }
    deleteMessage(){
        this.props.deleteMessage(this.props.message.id);

    }
    selectMessageForEdit(){
        this.props.editTrigger(this.props.message.id,this.props.message.text);
    }
    render(){
        const p = this.props.message;
        let tools;
        let user = false;
        if(this.props.isOwn)
            tools =(<div className = "tools">
                    <i onClick = {this.selectMessageForEdit} className = "message-edit fas fa-pencil-alt"></i>
                    <i onClick = {this.deleteMessage} className="fas fa-trash-alt message-delete"></i>
                </div>);
        else{
            tools =( <div className = "tools">
                        <div className = "byrger"></div>
                        <i onClick ={this.onClick} className ={(this.state.isLiked?"message-liked":"message-like" )+ " fas fa-heart" }></i>
                    </div>)
            user = (
                    <div className = "user-message-wrp">
                        <img src = {p.avatar} alt = {"user-img-"+p.user}/> 
                        <div className ="message-user-name">{p.user}</div>
                    </div>
                )
        }

        let date = this.converDate(new Date(p.createdAt));
        if(p.editedAt){
            date = "edited at "+this.converDate(new Date(p.editedAt));
        }
        return(
            <div className = {this.props.isOwn?"own-message":"message"}>
                <div className = "message-wrp">

                        {user}
                    <div className = "main-message-wrp">
                        <p className ="message-text">{p.text} </p>
                        <div className ="message-time">{date}</div>
                    </div>
                    {tools}
                </div>
            </div>
        );
    }
}
