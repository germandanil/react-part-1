import React from "react"
export default class Header extends React.Component{
    converDate(date){
        const mounthZero = date.getMonth()+1<10?"0":""
        const dateZero = date.getDate()+1<10?"0":""
        const hoursZero = date.getHours()<10?"0":""
        const minutesZero = date.getMinutes()<10?"0":""
        return `${ dateZero+date.getDate()}.${mounthZero+(date.getMonth())}.${date.getFullYear()} ${hoursZero+date.getHours()}:${minutesZero+date.getMinutes()}`
    }
    render(){
        const messages = this.props.messages??[];
        const numMessage = messages.length??0;
        let date = "no one has written yet"
        if(numMessage!==0)
            date = this.converDate(new Date(messages[numMessage-1].createdAt));
        const users = new Set();

        messages.forEach(m => {
            users.add(m.userId)
        });
        const numUsers = users.size
        return (
            <div className= "header">
                <div className = "header-title">{this.props.title}</div>
                <div className = "header-users-count">{numUsers + " participants"} </div>
                <div className = "header-messages-count">{numMessage+ " messages"}</div>
                <div className = "header-last-message-date">{"last message at   "+date}</div>      
            </div>
        )
    }
}
