import React from "react";
import Message from "./Message"
import DateLine from "./Dateline";
export default class MessageList  extends React.Component{
    render(){
        const messages =this.props.messages??[];
        const ownID = this.props.ownID??0;
        let list = [];
        const dayMilliseconds = 24*60*60*1000;
        let lastDate = new Date(Date.parse(messages[messages.length-1].createdAt) - dayMilliseconds) // date before oldest message
        let currentData;
        messages.forEach(m => {
            currentData = new Date(m.createdAt)
            if(lastDate.getDate() !==currentData.getDate()  || lastDate.getFullYear() !==currentData.getFullYear()||lastDate.getMonth() !==currentData.getMonth() ){
                list.push(<DateLine key = {currentData.toLocaleString('en-US')} date = {currentData}/>)
                lastDate = currentData;
            }

            list.push(<Message editTrigger = {this.props.editTrigger} deleteMessage = {this.props.deleteMessage} key= {m.id} 
                message = {m} isOwn = {ownID===m.userId}/>);
        });
        return(
            <div className = "message-list">
                {list}
            </div>
        )
    }
}