import React from "react";
import Preloader from "./Preloader";
import Header from "./Header";
import MessageList from "./Messagelist";
import MessageInput from "./Input"

var _ = require('lodash');

const MY_ID = "5328dba1-1b8f-11e8-9629-c7eca82aa7bd"
const MY_NAME = "Ben"
const MY_SRC_AVA = "https://www.aceshowbiz.com/images/photo/tom_pelphrey.jpg"

class Chat extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            preloader: true,
            lastUsedID: 0,
            editStatus:false
        }
        this.deleteMessageById = this.deleteMessageById.bind(this);
        this.createNewMessage = this.createNewMessage.bind(this);
        this.selectMessageForEdit = this.selectMessageForEdit.bind(this);
        this.editMessage = this.editMessage.bind(this);
    }
    componentDidMount() {
        fetch(this.props.url).then(
            res=>res.json()
        ).then(
            resArray=>{
                let messagesArray = resArray.map(m =>{  
                    m.show = true;
                    return m
                })
                messagesArray = _.sortBy(messagesArray, [(m)=> {return Date.parse(m.createdAt)} ]);
                this.setState({preloader: false, messages: messagesArray})

            })
    }
    deleteMessageById(id){
        _.remove(this.state.messages,(m)=>m.id===id);
        this.setState({messages:this.state.messages,editStatus:false})
    }
    createNewMessage(textMessage,dateStr){
        const message = {userId:MY_ID,
            user:MY_NAME,
            text:textMessage,
            src: MY_SRC_AVA,
            createdAt:dateStr,
            id:this.state.lastUsedID+1
        }

        this.state.messages.push(message);
        this.setState({messages:this.state.messages,lastUsedID:this.state.lastUsedID+1})
    }
    selectMessageForEdit(id,startText){
        this.setState({selectedID:id,editStatus:true,startEditText:startText});
    }
    editMessage(editText,dateStr){
        const messages = this.state.messages
        const message = _.find(messages,{id:this.state.selectedID});
        message.text = editText;

        message.editedAt = dateStr;

        this.setState({messages:messages,editStatus:false})
    }
    render(){
        const preloader = this.state.preloader;
        let content =[<Preloader key = "Preloader"/>];
        let status = this.state.editStatus;
        if(!preloader){
            content = [
                    <Header key = "Header" messages = {this.state.messages} title = "Hidden pull"/>,
                    <MessageList editTrigger = {this.selectMessageForEdit} deleteMessage = {this.deleteMessageById} key = "MessageList" 
                    messages = {this.state.messages} ownID = {MY_ID}/>,
                    <MessageInput startText= {status?this.state.startEditText:""} editStatus = {status} 
                    submitText = {status?this.editMessage:this.createNewMessage} key = "MessageInput" />
            ]
        }
        return (
        <div className = "chat">
            {content}
        </div>
        );
    }
}

export default Chat;