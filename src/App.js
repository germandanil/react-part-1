import Chat from './components/Chat.js';
import src from './data.js';
function App() {
  return (
    <Chat url = {src}/>
  );
}

export default App;
